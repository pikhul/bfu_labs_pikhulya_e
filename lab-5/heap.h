#ifndef __HEAP_
#define __HEAP_
#include <stdio.h>
#include <stdlib.h>
#define MAX_SIZE 100




// Element of heap
typedef struct
{
    int priority;
    void* data;
} element;

typedef int (*FCmp)(int,void*,int,void*);
//Default comparing
int GreaterPriority (int p1, void* d1, int p2, void* d2);

int LessPriority (int p1, void* d1, int p2, void* d2);

typedef void* (*FCp) (void*);
typedef void (*FDlt) (void*);

//Binary Heap
typedef struct
{
    element* mass;
    int currentSize;
    FCmp Compare;
    FCp Copy;
    FDlt Delete;
} THeap;

//Crating an empty heap
THeap * CreateHeap (FCmp Cmp, FCp Cp, FDlt Dlt);
//Clearing the heap
void DestructHeap ( THeap * H);
//Adding custom element
void AddElement ( THeap * Heap, int p, void* d);
//Adding elements from array
void AddArrayOfElements (THeap * Heap, int* ArrayP, void** ArrayD, int n);
//Changing priority
void ChangePriority (THeap * Heap, int i, int x);
//Getting data of element with max priority
void* GetMaxElementData(THeap * Heap);
//heapsort
void HeapSort (int * ArrayP, void ** ArrayD, int n, FCmp Compare, FCp Copy, FDlt Delete);

#endif
