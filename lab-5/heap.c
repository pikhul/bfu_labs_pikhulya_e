#include "heap.h"


THeap * CreateHeap (FCmp Cmp, FCp Cp, FDlt Dlt)
{
    THeap * NewHeap;
    NewHeap = calloc( 1, sizeof(THeap));
    NewHeap->mass = calloc(MAX_SIZE, sizeof(element));
    NewHeap->Compare = Cmp;
    NewHeap->Copy = Cp;
    NewHeap->Delete = Dlt;
    return NewHeap;
}

void DestructHeap ( THeap * H)
{
    int i;
    for (i = 0; i<H->currentSize; i++)
    {
        H->Delete(H->mass[i].data);
    }
    free(H->mass);
    free(H);
}


int GreaterPriority (int p1, void* d1, int p2, void* d2)
{
    if (p1 > p2) return 1;
    if (p1 == p2) return 0;
    if (p1 < p2) return -1;
}

int LessPriority (int p1, void* d1, int p2, void* d2)
{
    if (p1 > p2) return -1;
    if (p1 == p2) return 0;
    if (p1 < p2) return 1;
}


static void Heapify(THeap * Heap, int i)
{
    element temp;
    int left = 2*i+1;
    int right = 2*i+2;
    if(left < Heap->currentSize)
    {
        if(Heap->Compare(Heap->mass[i].priority,Heap->mass[i].data, Heap->mass[left].priority,Heap->mass[left].data)!=1)
        {
            temp = Heap->mass[i];
            Heap->mass[i] = Heap->mass[left];
            Heap->mass[left] = temp;
            Heapify(Heap, left);
        }
    }
    if(right < Heap->currentSize)
    {
        if(Heap->Compare(Heap->mass[i].priority,Heap->mass[i].data, Heap->mass[right].priority,Heap->mass[right].data)!=1)
        {
            temp = Heap->mass[i];
            Heap->mass[i] = Heap->mass[right];
            Heap->mass[right] = temp;
            Heapify(Heap, right);
        }
    }

}


void AddElement ( THeap * Heap, int p, void* d)
{

    int i = Heap->currentSize;
    Heap->mass[i].data = Heap->Copy(d);
    Heap->mass[i].priority = p;
    if (i!=0)
    {
        int parent = (i-1)/2;
        while(parent >= 0 && i > 0)
        {
            if(Heap->Compare(Heap->mass[i].priority,Heap->mass[i].data,Heap->mass[parent].priority,Heap->mass[parent].data)==1)
            {
                element temp = Heap->mass[i];
                Heap->mass[i] = Heap->mass[parent];
                Heap->mass[parent] = temp;
            }
            i = parent;
            parent = (i-1)/2;
        }
    }
    Heap->currentSize ++;
}

void AddArrayOfElements (THeap * Heap, int* ArrayP, void** ArrayD, int n)
{
    int i;
    for (i = 0; i < n; i++)
    {
        AddElement(Heap, ArrayP[i], ArrayD[i]);
    }
}




void ChangePriority (THeap * Heap, int i, int x)
{

    Heap->mass[i].priority = x;
    Heapify(Heap,0);

}

static element GetMaxElement(THeap * Heap)
{
    element x;
    x.data = Heap->Copy(Heap->mass[0].data);
    Heap->Delete(Heap->mass[0].data);
    x.priority = Heap->mass[0].priority;
    Heap->currentSize--;
    Heap->mass[0] = Heap->mass[Heap->currentSize];
    Heapify(Heap, 0);
    return(x);
}


void* GetMaxElementData(THeap * Heap)
{
    return(GetMaxElement(Heap).data);
}


void HeapSort (int * ArrayP, void ** ArrayD, int n, FCmp Compare, FCp Copy, FDlt Delete)
{
    int i;
    THeap * Sort = CreateHeap(Compare, Copy, Delete);
    AddArrayOfElements (Sort, ArrayP,ArrayD, n);
    for (i = 0; i < n; i++)
    {
        Sort->Delete(ArrayD[i]);
    }
    Heapify(Sort, 0);
    for (i = 0; i < n; i++)
    {
        element tmp = GetMaxElement(Sort);
        ArrayP[i] = tmp.priority;
        ArrayD[i] = tmp.data;
    }
    DestructHeap(Sort);
}

