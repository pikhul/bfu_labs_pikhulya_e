#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#define MAX_SIZE 100
#include "heap.h"


void* copyInt(void* X){
        int* res=malloc(sizeof(int));
        *res=*(int*)X;
        return res;
}

void deleteInt (void* X)
{
    free(X);
}

int main(){

    const N = 6;

    int i;
    int* Arrp = malloc(sizeof(int)*N);
    void** Arrd = malloc(sizeof(int*)*N);

    for ( i = 0; i < N; i ++)
    {
        Arrp[i] = N-1-i;
        Arrd[i] = malloc(sizeof(int));
        *(int*)(Arrd[i]) = (Arrp[i])*20;
    }

    for ( i = 0; i < N; i ++) printf("%d %d\n",Arrp[i], *(int*)Arrd[i]);

    HeapSort(Arrp, Arrd, N, LessPriority, copyInt, deleteInt);

    for ( i = 0; i < N; i ++)printf("%d %d\n",Arrp[i], *(int*)Arrd[i]);

    for ( i = 0; i < N; i ++)
    {
        free(Arrd[i]);
    }
    free(Arrp);
    free(Arrd);
}
