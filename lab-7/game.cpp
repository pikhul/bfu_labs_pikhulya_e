#include "game.h"



void Passive::Set_flag(int value)
{
    _flag = value;
}

int Passive::Check(Unit* owner)
{
    int flag = rand()%3;
    switch (_flag)
    {
    case 0:
        cout << "[Checking \"Fear\"...]\n";
        return 1;
    case 1:
        cout << "[Checking \"Magic Shield\"...]\n";
        if (flag == 0) return 1;
        else return 0;
    case 2:
        cout << "[Checking \"Naked Knight\"...]\n";
        if (owner->Get_def() - owner->Get_damage() <= 0 && owner->Get_hp() == owner->Get_maxhp())
            return 1;
        else return 0;
    }
}

void Passive::Use(Unit* owner)
{
    switch (_flag)
    {
    case 0:
        if (owner->Get_hp() >= (owner->Get_maxhp()/2))
        {
            cout << "Goblin hits himself! Got damaged twice!\n";
            if (owner->Get_def() > 0) owner->Reduce_def(owner->Get_damage()*2);
            else owner->Reduce_hp(owner->Get_damage()*2);
        }
        else
        {
            cout << "Goblin dodges! Goblin got half damage.\n";
            if (owner->Get_def() > 0) owner->Reduce_def(owner->Get_damage()/2);
            else owner->Reduce_hp(owner->Get_damage()/2);
        }
        break;
    case 1:
        cout << "Magic Shield worked. Mage got no damage.\n";
        owner->Reduce_mp(owner->Get_damage()*2);
        break;
    case 2:
        cout << "Knight lost his defense. Attack increases.\n";
        owner->Reduce_def(owner->Get_damage());
        owner->Increase_atc(owner->Get_atc());
        break;
    }
}

void Active::Set_flag(int value)
{
    _flag = value;
}


void Active::Use(Unit* owner)
{
    int flag = rand()%4;
    switch (_flag)
    {
    case 0:
        owner->bomb ++;
        break;
    case 1:
        cout << "Mage casts a spell... ";
        if (owner->Get_mp()>= 20)
        {
                owner->Reduce_mp(20);
            switch (flag)
            {
            case 0:
                cout << "HP increase!\n";
                owner->Increase_hp(owner->Get_hp());
                break;
            case 1:
                cout << "MP increase!\n";
                owner->Increase_mp(owner->Get_mp());
                break;
            case 2:
                cout << "Defense increases!\n";
                owner->Increase_def(2);
                break;
            case 3:
                cout << "Something went wrong! Mage damaged himself!\n";
                owner->Reduce_hp(owner->Get_hp()-1);
                break;
            }
        }
        else cout << "Not enough MP.\n";
        break;
    case 2:
        if (owner->Get_def() >=3)
        {
            cout << "Knight eats his own armor! Attack increases.\n";
            owner->Increase_atc(1);
            cout << owner->Get_def();
            owner->Reduce_def(2);
        }
        else cout << "Knight could not eat his armor. Not enough DEF.\n";
        break;
    }
}


Unit::Unit (int hp, int mp, int def, int atc, int dist, int mode)
{
    _MAX_HP = hp;
    _MAX_MP = mp;
    _cur_hp = _MAX_HP;
    _cur_mp = _MAX_MP;
    _cur_def = def;
    _atc = atc;
    _dist = dist;
    _status = 1;
    _cur_damage = 0;
    _place = 0;
    _passive.Set_flag(mode);
    _active.Set_flag(mode);
    bomb = 0;

}

int Unit::Get_hp()
{
    return _cur_hp;
}

int Unit::Get_mp()
{
    return _cur_mp;
}

int Unit::Get_def()
{
    return _cur_def;
}

int Unit::Get_atc()
{
    return _atc;
}

int Unit::Get_dist()
{
    return _dist;
}

int Unit::Get_status()
{
    return _status;
}

int Unit::Get_maxhp()
{
    return _MAX_HP;
}

int Unit::Get_maxmp()
{
    return _MAX_MP;
}

int Unit::Get_damage()
{
    return _cur_damage;
}

int Unit::Get_place()
{
    return _place;
}

void Unit::Reduce_hp(int points)
{
    _cur_hp = _cur_hp - points;
    if (_cur_hp <= 0)
    {
        _status = 0;
        _cur_hp = 0;
    }
}

void Unit::Reduce_def(int points)
{
    cout << Get_def();
    if (_cur_def>= points)
        _cur_def = _cur_def - points;
    else
    {
        cout << Get_def();
        Reduce_hp(points - _cur_def);
        _cur_def = 0;
        cout << Get_def();
    }
}

void Unit::Reduce_mp(int points)
{
    if (_cur_mp > 0)
        _cur_mp = _cur_mp - points;
    if (_cur_mp<0)
        _cur_mp = 0;
}

void Unit::Increase_hp(int points)
{
    _cur_hp+=points;
    if (_cur_hp > _MAX_HP) _cur_hp = _MAX_HP;
}

void Unit::Increase_mp(int points)
{
    _cur_mp+=points;
    if (_cur_mp > _MAX_MP) _cur_mp = _MAX_MP;
}

void Unit::Increase_def(int points)
{
    _cur_def+=points;
}
void Unit::Increase_atc(int points)
{
    _atc+=points;
}

void Unit::Damaged(int points)
{
    _cur_damage = points;
    if (_status == 1)
    {
        if (_passive.Check(this) == 0)
        {
            if (_cur_def>0) Reduce_def(points);
            else Reduce_hp(points);
        }
        else _passive.Use(this);
    }
    else cout << "Player is already dead.";

}

void Unit::Set_place(int point)
{
    _place+=point;
}

void Unit::Use_active()
{
    _active.Use(this);
}


void Unit::Show_stats()
{
    cout << "    HP:";
    cout << _cur_hp;
    cout << "/";
    cout << _MAX_HP;
    cout << "\n";
    cout << "    MP:";
    cout << _cur_mp;
    cout << "/";
    cout << _MAX_MP;
    cout << "\n";
    cout << "    DEF:";
    cout << _cur_def;
    cout << "\n";
    cout << "    ATC:";
    cout << _atc;
    cout << "\n";
    cout << "    place:";
    cout << _place;
    cout << "\n";
}

Arena::Arena(Unit *unit1, Unit *unit2)
{
    srand(time(NULL));
    cout << "\nDuel!\n";
    _fighter1 = unit1;
    _fighter2 = unit2;
    _fighter1->Set_place(0);
    _fighter2->Set_place(6);
    Update();
}

void Arena::Hit(Unit *unit1, Unit *unit2)
{
    if (unit1->Get_status() == 1 && unit2->Get_status() == 1)
    {
        if ((abs(unit1->Get_place()-unit2->Get_place()) <= unit1->Get_dist()) && (unit2->Get_status()!=0))
        {
            cout << "Attack!\n";
            unit2->Damaged(unit1->Get_atc());
        }
    }
    else cout << "Player is already dead.\n";
    Update();
}

void Arena::Move(Unit *unit, int point)
{
    if (unit->Get_status() == 1)
    {
        cout << "Moving...\n";
        if ((unit->Get_place()+point)!= _fighter1->Get_place() && (unit->Get_place()+point)!= _fighter2->Get_place() && (unit->Get_place()+point)<=6 && (unit->Get_place()+point)>=0)
            unit->Set_place(point);
            else cout << "Could not move.\n";
    }
    else cout << "Player is already dead.\n";
    Update();
}

void Arena::Use_skill(Unit *unit)
{
    if (unit->Get_status() == 1)
    {
        unit->Use_active();
        if (unit->bomb == 2)
        {
            cout << "Goblin's bomb has exploded!!!\n";
            _fighter1->Damaged(1);
            _fighter2->Damaged(1);
            unit->bomb = 0;
        }
        if (unit->bomb == 1) cout << "Goblin's bomb has been set up!\n";
    } else cout << "Player is already dead.\n";
    Update();
}

void Arena::Update()
{
    int temp;
    cout << "Player 1:\n";
    _fighter1->Show_stats();
    cout << "Player 2:\n";
    _fighter2->Show_stats();
    cout << "\n\n";
    cin.get();
}

int Arena::Check_Deaths()
{
    int f = 0;
    if (_fighter1->Get_status() == 0)
    {
        cout << "Player 1 has died.\n";
        f = 1;
    }
    if (_fighter2->Get_status() == 0)
    {
        cout << "Player 2 has died.\n";
        f = 1;
    }
    return f;
}
