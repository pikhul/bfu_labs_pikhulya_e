#ifndef GAME_INCLUDED
#define GAME_INCLUDED
#include <iostream>
#include <cstdlib>
#include <cstdio>
#include <ctime>
#include <cmath>

using namespace std;

class Unit;

class Passive
{
private:
    int _flag;
public:
    void Set_flag(int value);
    int Check(Unit* owner) ;
    void Use(Unit* owner) ;
};

class Active
{
private:
    int _flag;
public:
    void Set_flag(int value);
    void Use(Unit* owner) ;
};


class Unit
{
protected:
    int _cur_hp;
    int _cur_mp;
    int _MAX_HP;
    int _MAX_MP;
    int _cur_def;
    int _atc;
    int _dist;
    int _status;
    int _cur_damage;
    Passive _passive;
    Active _active;
    int _place;
public:
    int bomb;
    Unit(){}
    Unit(int hp, int mp, int def, int atc, int dist, int mode);
    Unit (Unit &unit);
    int Get_hp();
    int Get_maxhp();
    int Get_mp();
    int Get_maxmp();
    int Get_def();
    int Get_atc();
    int Get_dist();
    int Get_status();
    int Get_damage();
    int Get_place();
    void Reduce_hp(int points);
    void Reduce_mp(int points);
    void Reduce_def(int points);
    void Increase_hp(int points);
    void Increase_mp(int points);
    void Increase_def(int points);
    void Increase_atc(int points);
    void Damaged(int points);
    void Set_place(int point);
    void Use_active();
    void Show_stats();
};

class Goblin : public Unit
{
public:
    Goblin():Unit(15, 0 , 0, 1, 1,0){};
};

class Mage : public Unit
{
public:
    Mage() : Unit(10,30 , 0, 2, 3,1){}
};

class Knight : public Unit
{
public:
    Knight() : Unit(10, 0 , 5, 2, 1,2){}
};

class Arena
{
private:
    Unit *_fighter1;
    Unit *_fighter2;
    void Update();
public:
    Arena (Unit *unit1, Unit *unit2);
    void Hit(Unit *unit1, Unit *unit2);
    void Move (Unit *unit, int point);
    void Use_skill (Unit *unit);
    int Check_Deaths();
};

#endif // GAME_INCLUDED
