#include "Factory.h"

Unit* Fabric:: operator()(string character)
{
    Unit* player;
    if (character == "Goblin")
    {
        player = new Goblin;
    }
    if (character == "Mage")
    {
        player = new Mage;
    }
    if (character == "Knight")
    {
        player = new Knight;
    }
    return player;
}