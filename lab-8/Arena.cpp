#pragma once
#include "Arena.h"

Arena::Arena(Unit *unit1, Unit *unit2)
{
    cout << "\nDuel!\n";
    _fighter1 = unit1;
    _fighter2 = unit2;
    _fighter1->Set_place(0);
    _fighter2->Set_place(6);
    Update();
}

void Arena::Update()
{
    int temp;
    cout << "Player 1:\n";
    _fighter1->Show_stats();
    cout << "Player 2:\n";
    _fighter2->Show_stats();
    cout << "\n\n";
    //cin.get();
}

void Arena::Hit(Unit *unit1, Unit *unit2)
{
    if (unit1->Get_status() != 1 || unit2->Get_status() != 1) cout << "Player is already dead.\n";
    else
    {
        if ((abs(unit1->Get_place() - unit2->Get_place()) <= unit1->Get_dist()) && (unit2->Get_status() != 0)) {
            cout << "Attack!\n";
            unit2->Damaged(unit1->Get_atc());
        }
    }
    Update();
}

void Arena::Move(Unit *unit, int point)
{
    if (unit->Get_status() == 1)
    {
        cout << "Moving...\n";
        if ((unit->Get_place()+point)!= _fighter1->Get_place() && (unit->Get_place()+point)!= _fighter2->Get_place() && (unit->Get_place()+point)<=6 && (unit->Get_place()+point)>=0)
            unit->Set_place(unit->Get_place()+point);
        else cout << "Could not move.\n";
    }
    else cout << "Player is already dead.\n";
    Update();
}

void Arena::Use_skill(Unit *unit)
{
    Unit** target = new Unit* [2];
    target[0] = unit;
    if (unit == _fighter1) target[1] = _fighter2;
    else target[1] = _fighter1;
    if (unit->Get_status() != 1) cout << "Player is already dead.\n";
    else unit->Use_active(1, target);
    Update();
    delete[] target;
}

int Arena::Check_Deaths()
{
    int f = 0;
    if (_fighter1->Get_status() == 0)
    {
        cout << "Player 1 has died.\n";
        f = 1;
    }
    if (_fighter2->Get_status() == 0)
    {
        cout << "Player 2 has died.\n";
        f = 1;
    }
    return f;
}