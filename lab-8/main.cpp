#include "Arena.cpp"
#include "Factory.cpp"

int main ()
{
    Fabric f;
    char command;
    Unit* player1;
    Unit* player2;
   /* Arena Duel(player1,player2);
    while (Duel.Check_Deaths() != 1)
    {
        cout << "\nPlayer 1: ";
        cin >> command;
        if (command == 'R') Duel.Move(player1, 1);
        if (command == 'L') Duel.Move(player1, -1);
        if (command == 'S') Duel.Use_skill(player1);
        if (command == 'A') Duel.Hit(player1, player2);
        cout << "\nPlayer 2: ";
        cin >> command;
        if (command == 'R') Duel.Move(player2, 1);
        if (command == 'L') Duel.Move(player2, -1);
        if (command == 'S') Duel.Use_skill(player2);
        if (command == 'A') Duel.Hit(player2, player1);
    }*/
    cout << "Hello!\n";
    cout << "You can choose 1 of 3 characters to play as.\n";
    cout << "Goblin:\n";
    cout << "   - Active skill: \"Bomb\" (can make and use a bomb on Arena) \n";
    cout << "   - Passive skill: \"Fear\" (gets damage twice or half of damage every time he gets attacked) \n";
    cout << "Mage:\n";
    cout << "   - Active skill: \"Magic\" (MP x2 or HP x2 or DEF +2 or HP = 1 equally likely) \n";
    cout << "   - Passive skill: \"Magic Shield\" (Gets no damage with ~30% chance) \n";
    cout << "Knight:\n";
    cout << "   - Active skill: \"Eating Armor\" (reducing DEF, but increasing ATC) \n";
    cout << "   - Passive skill: \"Naked Knight\" (ATC x2 when DEF = 0) \n";
    while (command != 'Q')
    {
        string character;
        cout << "Player 1:\n";
        cin >> character;
        player1 = f(character);
        cin >> character;
        cout << "Player 2:\n";
        player2 = f(character);
        cout << "[CONTROL:\n";
        cout << "    To move right press \"R\".\n";
        cout << "    To move left press \"L\".\n";
        cout << "    To attack press \"A\".\n";
        cout << "    To use skill press \"S\".\n";
        cout << "Good luck!]\n\n";

        Arena Duel(player1, player2);
        while (Duel.Check_Deaths() != 1)
        {
            cout << "\nPlayer 1: ";
            cin >> command;
            if (command == 'R') Duel.Move(player1, 1);
            if (command == 'L') Duel.Move(player1, -1);
            if (command == 'S') Duel.Use_skill(player1);
            if (command == 'A') Duel.Hit(player1, player2);
            cout << "\nPlayer 2: ";
            cin >> command;
            if (command == 'R') Duel.Move(player2, 1);
            if (command == 'L') Duel.Move(player2, -1);
            if (command == 'S') Duel.Use_skill(player2);
            if (command == 'A') Duel.Hit(player2, player1);
        }
        cout << "\nTo exit press \"Q\".";
        cout << "\nAny key to continue.";
        cin >> command;
    }
    return 0;
}