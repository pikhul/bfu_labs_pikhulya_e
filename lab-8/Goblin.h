#pragma once
#ifndef GOBLIN_H
#define GOBLIN_H
#include "Bomb.cpp"
#include "Panic.cpp"

class Goblin : public Unit
{
    public:
        Goblin(): Unit()
        {
            _skill.push_back(new Panic);
            _skill.push_back(new Bomb);
        }
        void Damaged(int points) ;
        void Use_active(int id, Unit** target) ;
    private:

};

#endif // GOBLIN_H
