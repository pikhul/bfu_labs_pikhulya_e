#include "EatingArmor.h"

void EatingArmor::Run (Unit** target)
{
    if (target[0]->Get_def() <3) cout << "Knight could not eat his armor. Not enough DEF.\n";
    else
    {
        cout << "Knight eats his own armor! Attack increases.\n";
        target[0]->Set_atc(target[0]->Get_atc()+1);
        target[0]->Set_def(target[0]->Get_def()-2);
    }
}
