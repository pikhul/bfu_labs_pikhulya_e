#ifndef KNIGHT_H
#define KNIGHT_H
#include "EatingArmor.cpp"
#include "NakedKnight.cpp"

class Knight : public Unit
{
    public:
        Knight() : Unit(12, 8, 0, 1)
        {
            _skill.push_back(new NakedKnight);
            _skill.push_back(new EatingArmor);
        }
        void Damaged(int points);
        void Use_active(int id, Unit** target) ;
    protected:
    private:
};

#endif // KNIGHT_H
