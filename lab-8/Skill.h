#pragma once
#ifndef SKILL_H
#define SKILL_H
#include "Unit.cpp"


class Skill
{
    public:
        Skill(){};
        //virtual ~Skill();
        virtual void Run (Unit** target) = 0;
        int a;
    protected:
    private:
};

#endif // SKILL_H
