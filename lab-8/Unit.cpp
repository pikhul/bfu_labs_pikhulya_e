#pragma once
#include "Unit.h"

Unit::Unit()
{
    _MAX_HP = 10;
    _MAX_MP = 0;
    _cur_hp = 10;
    _cur_mp = 0;
    _cur_def = 0;
    _atc = 1;
    _dist = 1;
    _status = 1;
    _cur_damage = 0;
    _place = 0;
    _bomb = 0;
    _skill.reserve(2);
}

Unit::Unit(int hp, int def, int mp, int dist)
{
    _MAX_HP = hp;
    _MAX_MP = mp;
    _cur_hp = hp;
    _cur_mp = mp;
    _cur_def = def;
    _atc = 1;
    _dist = dist;
    _status = 1;
    _cur_damage = 0;
    _place = 0;
    _bomb = 0;
    _skill.reserve(2);
}

Unit::~Unit()
{
}

int Unit::Get_hp()
{
    return _cur_hp;
}

int Unit::Get_mp()
{
    return _cur_mp;
}

int Unit::Get_def()
{
    return _cur_def;
}

int Unit::Get_atc()
{
    return _atc;
}

int Unit::Get_dist()
{
    return _dist;
}

int Unit::Get_status()
{
    return _status;
}

int Unit::Get_maxhp()
{
    return _MAX_HP;
}

int Unit::Get_maxmp()
{
    return _MAX_MP;
}

int Unit::Get_damage()
{
    return _cur_damage;
}

int Unit::Get_place()
{
    return _place;
}

int Unit::Get_bomb()
{
    return _bomb;
}

void Unit::Set_hp(int points)
{
    if (points > _MAX_HP) _cur_hp = _MAX_HP;
    else if (points <=0)
    {
        _cur_hp = 0;
        _status = 0;
    }
    else _cur_hp = points;
}

void Unit::Set_mp(int points)
{
    if (points> _MAX_MP) _cur_mp = _MAX_MP;
    else if (points <0) _cur_mp = 0;
    else _cur_mp = points;
}
void Unit::Set_def(int points)
{
    if (points < 0) _cur_def = 0;
    else _cur_def = points;
}
void Unit::Set_atc(int points)
{
    _atc = points;
}
void Unit::Set_damage(int points)
{
    _cur_damage = points;
}
void Unit::Set_place(int points)
{
    _place = points;
}

void Unit::Set_bomb(int points)
{
    _bomb = points;
}

void Unit::Damaged (int points)
{
    if (this->Get_def() > 0)
    {
        if (this->Get_def() < points)
        {
            this->Set_hp(this->Get_hp()-(this->Get_damage()-Get_def()));
            Set_def(0);
        }
        else this->Set_def(this->Get_def()-this->Get_damage());
    }
    else this->Set_hp(this->Get_hp()-this->Get_damage());
}

void Unit::Show_stats()
{
    cout << "    HP:";
    cout << _cur_hp;
    cout << "/";
    cout << _MAX_HP;
    cout << "\n";
    cout << "    MP:";
    cout << _cur_mp;
    cout << "/";
    cout << _MAX_MP;
    cout << "\n";
    cout << "    DEF:";
    cout << _cur_def;
    cout << "\n";
    cout << "    ATC:";
    cout << _atc;
    cout << "\n";
    cout << "    place:";
    cout << _place;
    cout << "\n";
}