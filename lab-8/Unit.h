#pragma once
#ifndef UNIT_H
#define UNIT_H
#include <cstdlib>
#include <ctime>
#include <iostream>
#include <vector>
#include <string>

using namespace std;

class Skill;
class Unit
{
public:
    Unit();
    Unit(int hp, int def, int mp, int dist);
    virtual ~Unit();
    int Get_hp();
    int Get_maxhp();
    int Get_mp();
    int Get_maxmp();
    int Get_def();
    int Get_atc();
    int Get_dist();
    int Get_status();
    int Get_damage();
    int Get_place();
    int Get_bomb();
    void Set_hp(int points);
    void Set_mp(int points);
    void Set_def(int points);
    void Set_atc(int points);
    void Set_damage(int points);
    void Set_place(int points);
    void Set_bomb(int points);
    virtual void Damaged(int points);
    virtual void Use_active(int id, Unit** target) =0;
    void Show_stats();
protected:
    vector <Skill*> _skill;
private:
    int _cur_hp;
    int _cur_mp;
    int _MAX_HP;
    int _MAX_MP;
    int _cur_def;
    int _atc;
    int _dist;
    int _status;
    int _cur_damage;
    int _place;
    int _bomb;
};

#endif // UNIT_H
