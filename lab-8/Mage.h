#ifndef MAGE_H
#define MAGE_H
#include "Magic_shield.cpp"
#include "Spell.cpp"

class Mage : public Unit
{
    public:
        Mage(): Unit (10, 0, 30, 2)
        {
            _skill.push_back(new Magic_shield);
            _skill.push_back(new Spell);
        }
        void Damaged(int points);
        void Use_active(int id, Unit** target) ;
    protected:
    private:
};

#endif // MAGE_H
