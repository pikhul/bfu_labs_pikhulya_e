#ifndef NAKEDKNIGHT_H
#define NAKEDKNIGHT_H
#include "Skill.h"

class NakedKnight : public Skill
{
    public:
        NakedKnight(){};
        void Run (Unit** target) ;
    protected:
    private:
};

#endif // NAKEDKNIGHT_H
