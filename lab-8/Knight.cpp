#include "Knight.h"


void Knight::Damaged(int points)
{
    Unit** target = new Unit* [1];
    target[0] = this;
    Set_damage(points);
    _skill[0]->Run(target);
    Unit::Damaged(points);
}

void Knight::Use_active(int id, Unit** target)
{
    _skill[id]->Run(target);
}
