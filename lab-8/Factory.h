#pragma once
#ifndef FABRIC_H
#define FABRIC_H
#include "Goblin.cpp"
#include "Mage.cpp"
#include "Knight.cpp"

class Fabric
{
    public:
        Fabric(){};
        Unit* operator ()(string character);
    private:

};

#endif // FABRIC_H
