#ifndef SPELL_H
#define SPELL_H
#include "Skill.h"

class Spell : public Skill
{
    public:
        Spell(){};
        void Run (Unit** target) ;
    protected:
    private:
};

#endif // SPELL_H
