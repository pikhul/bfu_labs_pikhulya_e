#ifndef MAGIC_SHIELD_H
#define MAGIC_SHIELD_H
#include "Skill.h"

class Magic_shield : public Skill
{
    public:
        Magic_shield(){};
        void Run (Unit** target) ;
    protected:
    private:
};

#endif // MAGIC_SHIELD_H
