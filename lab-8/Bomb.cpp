#include "Bomb.h"

void Bomb::Run (Unit** target)
{
    int f = target[0]->Get_bomb();
    switch (f)
    {
        case 0:
            target[0]->Set_bomb(1);
            cout << "Setting bomb...\n";
            break;
        case 1:
            cout << "Explosion...\n";
            target[0]->Damaged(1);
            target[1]->Damaged(1);
            target[0]->Set_bomb(0);
            break;
    }

}
