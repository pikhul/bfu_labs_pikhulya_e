#include "Goblin.h"

void Goblin::Damaged(int points)
{
    Unit** target = new Unit* [1];
    target[0] = this;
    Set_damage(points);
    Use_active(0, target);
    Unit::Damaged(points);
    delete[] target;
}


void Goblin::Use_active(int id, Unit** target)
{
    Bomb bomb;
    Panic panic;
    Skill *pBomb = &bomb;
    Skill *pPanic = &panic;
    _skill[1] = pBomb;
    _skill[0] = pPanic;
    _skill[id]->Run (target);
}
