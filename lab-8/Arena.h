#ifndef Arena_H
#define Arena_H
#include "Unit.cpp"

class Arena
{
    private:
        Unit* _fighter1;
        Unit* _fighter2;
        void Update();
    public:
        Arena (Unit* unit1, Unit* unit2);
        void Hit(Unit* unit1, Unit* unit2);
        void Move (Unit* unit, int point);
        void Use_skill (Unit *unit);
        int Check_Deaths();
};

#endif // Arena_H
