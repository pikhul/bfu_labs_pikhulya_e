#pragma once
#ifndef BOMB_H
#define BOMB_H
#include "Skill.cpp"

class Bomb : public Skill
{
    public:
        Bomb(){};
        void Run (Unit** target) ;
    protected:
    private:
};

#endif // BOMB_H
