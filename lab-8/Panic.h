#pragma once
#ifndef PANIC_H
#define PANIC_H
#include "Skill.h"

class Panic : public Skill
{
    public:
        Panic(){};
        void Run (Unit** target) ;
    protected:
    private:
};

#endif // PANIC_H
