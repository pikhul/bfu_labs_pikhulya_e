#include <iostream>
#include "thread_guard.h"
#include <vector>
#include <algorithm>
#include <cstdlib>
#include <ctime>
#include "thread_guard.h"

vector<int> a ;
thread_guard sorter;

void thread_qsort (int L, int R)
{
    srand(time(NULL));
    int i = L, j = R, x = a[L + rand() % (R - L)];
    while (i <= j)
    {
        while (a[i] < x) i++;
        while (a[j] > x) j--;
        if (i <= j)
        {
            int tmp  = a[i];
            a[i] = a[j];
            a[j] = tmp;
            i++;
            j--;
        }
    }
    if (L < j)
        if (!sorter.push(thread (thread_qsort, L, j))) thread_qsort(L, j);
    if (i < R) thread_qsort(i, R);
}

int main()
{
    a.push_back(14);
    a.push_back(14);
    a.push_back(14);
    a.push_back(115);
    a.push_back(14);
    thread_qsort(0, (a.size()-1));
    for(int i=0;i<a.size();i++)
    {
        cout << a[i];
    }
    return 0;
}