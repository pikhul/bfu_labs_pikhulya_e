#include "thread_guard.h"

thread_guard::thread_guard() {}
void thread_guard::join()
{
    for(int i=0;i<t.size();i++){
        if (t[i].joinable()) t[i].join();
    }
}
thread_guard::~thread_guard()
{
    join();
}

void thread_guard::push(thread in)
{
    this->t.push_back(move(in));
}
