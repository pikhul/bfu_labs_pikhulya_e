#include "shared_mutex.h"


void shared_mutex::lock_shared() {
    //mutex::lock();
    counter++;
    //mutex::unlock();
}

void shared_mutex::unlock_shared() {
    counter--;
}

void shared_mutex::lock() {
    while (true){
        mutex::lock();
        if (!counter){
            break;
        }
        mutex::unlock();
    }
}

void shared_mutex::unlock() {
    mutex::unlock();
}
