#include <mutex>
#include "shared_mutex.h"
#include <iostream>
#include <fstream>
#include <ctime>
#include "thread_guard.h"

shared_mutex _m;
int write_count = 0;

void write (int a) {
    _m.lock();
    write_count ++;
    std::this_thread::sleep_for(std::chrono::milliseconds(a));
    _m.unlock();
}

void read (int a)
{
    _m.lock_shared();
    std::this_thread::sleep_for(std::chrono::milliseconds(a));
    _m.unlock_shared();
}

void work ()
{
    //int seed = std::rand();
    int n = 50;
    while (n > 0) {
        std::srand(time(NULL));
        int d = std::rand() % 700;
        if (d > 300)
        {
            read(d);
        }
        else
        {
            write(d);
        }
        n --;
    }

}



int main()
{
    int a = 0;

    int NT = thread::hardware_concurrency();
    if(NT<2)
        NT=2;
    auto start = chrono::steady_clock::now();
    thread_guard worker;
    for (int i = 0; i < NT; i++)
    {
        worker.push(thread(work));
    }
    worker.join();
    cout <<" WRITE: " << write_count << " READ: " << 200 - write_count << endl;
    auto end = chrono::steady_clock::now();
    auto diff = end - start;
    cout << chrono::duration <double, milli> (diff).count() << " ms" << endl;
    return 0;
}