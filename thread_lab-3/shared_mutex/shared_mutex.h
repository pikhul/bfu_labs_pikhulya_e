#ifndef LAB3_SHARED_MUTEX_H
#define LAB3_SHARED_MUTEX_H

#include <mutex>
//#include <thread>
#include <atomic>

class shared_mutex : private std::mutex {
private:
    std::atomic<int> counter;
public:
    void lock();
    void unlock();
    void lock_shared();
    void unlock_shared();
};


#endif //LAB3_SHARED_MUTEX_H
