#include <iostream>
#include "priority_mutex.h"
#include <fstream>
#include "thread_guard.h"

priority_mutex high_level_mtx(1000);
priority_mutex middle_level_mtx (100);
priority_mutex low_level_mtx(10);

int a = 0;

void middle_priopity_func (ofstream& out)
{
    middle_level_mtx.lock();
    out <<"middle priority function proceed"<< std::endl;
    a+= 100;
    middle_level_mtx.unlock();
}
void high_priopity_func (ofstream& out)
{
    high_level_mtx.lock();
    out <<"high priority function proceed"<< std::endl;
    a+= 1000;
    middle_priopity_func ( out);
    high_level_mtx.unlock();
}

void low_priopity_func (ofstream& out)
{
    low_level_mtx.lock();
    out <<"low priority function proceed"<< std::endl;
    a+= 10;
    high_priopity_func (out);
    low_level_mtx.unlock();
}

void wrong_work(ofstream& out)
{
    low_priopity_func(out);
}

void right_work(ofstream& out)
{
    high_priopity_func(out);
}

int main()
{
    ofstream out1 ("log1.txt");
    ofstream out2 ("log2.txt");
    thread_guard worker;
    worker.push(thread(wrong_work, ref(out1)));
    worker.push(thread(right_work, ref(out2)));
    worker.join();
    cout<< a;
    return 0;
}