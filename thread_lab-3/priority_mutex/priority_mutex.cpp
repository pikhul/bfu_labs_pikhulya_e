#include <iostream>
#include "priority_mutex.h"

std::map<std::thread::id, std::stack<int>> priority_mutex::threads_levels;

priority_mutex::priority_mutex(int level) : std::mutex()
{
    this->level = level;
}

void priority_mutex::lock()
{
    std::stack<int>& thread_priorities = this->get_thread_priorities();
    int current_level = 0;
    if (!thread_priorities.empty())
        current_level = thread_priorities.top();
    try {
        if (check_priority(!thread_priorities.empty(), current_level)) {
            mutex::lock();
            thread_priorities.push(this->level);
        }else{
            throw 1;
        }
    }
    catch (int a)
    {
        std::cout << "Try to lock inappropriate mutex level!"<< std::endl;
        _endthread();
    }
}

void priority_mutex::unlock()
{
    std::stack<int>& thread_priorities = this->get_thread_priorities();
    thread_priorities.pop();
    mutex::unlock();
}

bool priority_mutex::check_priority(const bool thread_status, const int thread_priority)
{
    return (this->level <= thread_priority || !thread_status);
}

std::stack<int> &priority_mutex::get_thread_priorities()
{
    return threads_levels[std::this_thread::get_id()];
}
