#ifndef PRIORITY_MUTEX_H
#define PRIORITY_MUTEX_H
#include <mutex>
#include <thread>
#include <map>
#include <stack>

class priority_mutex:public std::mutex
{
private:
    int level;
    static std::map<std::thread::id, std::stack<int>> threads_levels;
    bool check_priority(bool thread_status, int thread_priority);
    std::stack<int>& get_thread_priorities();
public:
    priority_mutex(int level);
    void lock();
    void unlock();
};


#endif //PRIORITY_MUTEX_H
