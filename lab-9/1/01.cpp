#include <cstdio>
#include <cstdlib>
#include <cstring>
#define N 100

char c;

void read (char* word, FILE* in)
{
    int n = 0;
    while ((c != ' ' ) && (c != EOF)&& (c!= 10))
    {
        word[n] = c;
        n++;
        c = getc(in);

    }
    word[n] = 0;
}

void read (int * num, FILE* in)
{
    int n = 1;
    while ((c != ' ' ) && (c != EOF) && (c!= 10))
    {
        num[n] = (static_cast<int>(c) - 48)*10;
        c = getc(in);
        num[n] += static_cast<int>(c) - 48;
        c = getc(in);
        n++;
    }
    num[0] = n-1;
}

int pal_check (char * s)//this one I've stolen
{
   int i,l;
   l = strlen(s);
   for(i=0; i<l/2; i++)
   {
     if ( s[i] != s[l-i-1] ) return 0;
   }
   return 1;
}

int pal_check (int * num)
{
   int i,l;
   l = num[0];
   for(i = 1; i<= l/2; i++)
   {
     if ( num[i] != num[l-i+1] ) return 0;
   }
   return 1;
}

void pallindrom (char * word)
{
    if (pal_check (word))
    {
        int l = strlen(word);
        char max = 0;
        for (int i = 0; i < l; i++)
        {
            if (word[i] >= max) max = word[i];
        }
        printf ("%c ", max);
    }
}

void pallindrom (int * num)
{
    if (pal_check (num))
    {
        int l = num[0];
        char max = 0;
        for (int i = 1; i <= l; i++)
        {
            if (num[i] >= max) max = num[i];
        }
        printf ("%d ", max);
    }
}

int asc_check (char* word)
{
    int f = 1;
    int l = strlen(word);
    for (int i = 0; i < (l-1); i++)
    {
        if (word[i] > word[i+1]) f = 0;
    }
    return f;
}

int asc_check (int* num)
{
    int f = 1;
    int l = num[0];
    for (int i = 1; i < l; i++)
    {
        if (num[i] > num[i+1]) f = 0;
    }
    return f;
}

void ascending_sequence (char* word)
{
    if (asc_check(word))
    {
        printf("%c ", word[(strlen(word))/2]);
    }
    else if (!(pal_check(word))) printf ("%s", word);
}

void ascending_sequence (int* num)
{
    if (asc_check(num))
    {
        int tmp = 0;
        for (int i = 1; i <= num[0]; i++) tmp += num[i];
        tmp = tmp / num[0];
        printf("%d ", tmp);
    }
    else if (!(pal_check(num)))
    {
        for (int i = 1; i <= num[0]; i++) printf ("%02d", num[i]);
        printf (" ");
    }
}

int main()
{
    FILE * in = fopen("1.in","r");
    FILE * out = fopen("1.out","w");
    char * word = new char[N];
    int * num = new int [N/2];

    while ((c = getc(in))!= EOF)
    {
        if (c > 57)
        {
            read (word, in);
            pallindrom (word);
            ascending_sequence (word);
        }
        else
        {
            read (num, in);
            pallindrom (num);
            ascending_sequence(num);
        }
    }
    delete [] word;
    delete [] num;
    return 0;
}
