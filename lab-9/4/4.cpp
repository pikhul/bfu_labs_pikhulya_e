#include <iostream>
#include <cstring>
#include <map>
using namespace std;

int main()
{
    int N, M;
    map <int,int> listOfDates;
    map<int,int>::iterator it;

    cin >> N;
    for (int i = 0; i < N; i++)
    {
        int tmp;
        cin >> tmp;
        listOfDates.insert ( pair<int,int>(tmp,0) );
    }

    cin >> M;

    for (int i =0; i < M; i++)
    {
        int K, counter = 0;
        map <int,int> studentList;
        cin >> K;
        for (int i = 0; i < K; i++)
        {
            int tmp;
            cin >> tmp;
            studentList.insert ( pair<int,int>(tmp,0) );
        }
        for (it = listOfDates.begin(); it != listOfDates.end() ; it++)
        {
            if (studentList.find(it->first) != studentList.end()) counter++;
        }
        cout << counter<< endl;
    }
    return 0;
}
