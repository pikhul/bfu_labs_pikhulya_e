#include <iostream>
#include <fstream>
#include <vector>
#include <queue>

//#define DEBUG

using namespace std;


 struct ver
{
    int x;
    int y;
} ;


int find (vector<ver> &input, ver &target)
{
    for (int i = 0; i < input.size(); i++)
    {
        if (input[i].x ==target.x && input[i].y ==target.y) return i;
    }
    return -1;
}

int main ()
{
    int g [100][100];
    ifstream input("C:\\Users\\1\\1.txt");
#ifdef DEBUG
    if (input.is_open())
        cout << "Success\n" << endl;
    else
    {
        cout << "Fail\n" << endl;
        return -1;
    }
#endif // DEBUG
    vector <ver> v(100);
    int n;
    input >> n;
    int m;
    input >> m;
    char in [n][m];
    int n_v=0;
    //entering picture
    for (int i = 0; i < n; i++)
    {
        for (int j = 0; j < m; j++)
        {
            input >> in[i][j];
            if (in[i][j] =='#')
            {
                v[n_v].x = i;
                v[n_v].y = j;
                n_v++;
            }
        }
        input.get();
    }
    //building graph
    for (int i = 0; i < n_v; i++)
    {
        if (in[v[i].x][v[i].y +1] == '#')
        {
            ver tmp;
            tmp.x = v[i].x;
            tmp.y = v[i].y +1;
            g [i][find(v, tmp)] = 1;
        }
        if (in[v[i].x+1][v[i].y] == '#')
        {
            ver tmp;
            tmp.x = v[i].x+1;
            tmp.y = v[i].y;
            g [i][find(v, tmp)] = 1;
        }
        if (in[v[i].x-1][v[i].y] == '#')
        {
            ver tmp;
            tmp.x = v[i].x-1;
            tmp.y = v[i].y;
            g [i][find(v, tmp)] = 1;
        }
        if (in[v[i].x][v[i].y-1] == '#')
        {
            ver tmp;
            tmp.x = v[i].x;
            tmp.y = v[i].y-1;
            g [i][find(v, tmp)] = 1;
        }
    }
    //breadth-first search
    queue <int> turn;
    vector <int> used(n_v);
    int result = 0;

    for (int j =0; j <n_v; j++)
    {
        if (used[j]!=1)
        {
            turn.push(j);
            used[j]= 1;
            while ( !turn.empty() )
            {
                int ind=turn.front();
                turn.pop();
                for ( int i=0; i < n_v; i++ )
                {
                    if ( g[ind][i]== 1 )
                    {
                        if (used[i]!= 1)
                        {
                            used[i]= 1;
                            turn.push(i);
                        }
                    }
                }
            }
            result++;
        }
    }



#ifdef DEBUG
    cout << "Graph:\n";
    for (int i = 0; i < n; i++)
    {
        for (int j = 0; j < m; j++)
            cout << in[i][j] << " ";
        cout << "\n";
    }
    cout << "\n";
    cout << "Vertexes:\n";
    for (int i =0; i < n_v; i++)
    {
        cout << v[i].x << ","<< v[i].y<< " ";
    }
    cout << "\n\n";
    cout << "Adjacency matrix:\n";
    for (int i = 0; i < n_v; i++)
    {
        for (int j = 0; j < n_v; j++)
            cout << g[i][j] << " ";
        cout << "\n";
    }
    cout << "\n";
    cout << "Used vertexes:\n";
    for (int i =0; i < n_v; i++)
    {
        cout << used[i] << " ";
    }
    cout << "\n";
#endif // DEBUG
    cout << "Result:\n"<< result;
    return 0;
}