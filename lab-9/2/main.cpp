#include "Baker.cpp"
#include <cstdio>
#include <iostream>

using namespace std;

void Level_up (Baker& player, int work)
{
    player.Set_exp(player.Get_exp()+(work/5));
    if (player.Get_exp()> 10)
    {
        player.Set_level(player.Get_level()+1);
        player.Set_exp(0);
    }
}

int Bake(Baker player1, Baker player2)
{
    cout << "Baking begins!\n";
    cout << "1 to bake, 2 to eat, 3 to share.\n";
    int f = 0;
    int work = 0;
    char command;
    int start_rols1 = player1.Get_current_roll();
    int start_rols2 = player2.Get_current_roll();
    while ((f < 25) && work <500)
    {
        cout << "Player 1\n";
        cin >> command;
        if (command == '1') player1.Create_roll();
        if (command == '2') player1.Eat_roll();
        if (command == '3') Baker::Give_roll(player1,player2);

        cout << "Player 2\n";
        cin >> command;
        if (command == '1') player2.Create_roll();
        if (command == '2') player2.Eat_roll();
        if (command == '3') Baker::Give_roll(player2,player1);

        int n = player1.Get_current_roll();
        for (int i = start_rols1; i < n; i++)
        {
            work+=player1.Get_roll(i).Get_taste();
        }

        n = player2.Get_current_roll();
        for (int i = start_rols2; i < n; i++)
        {
            work+=player2.Get_roll(i).Get_taste();
        }
        f++;
    }
    if (work < 500) cout << "You loose.\n"<< work << " exp eared.\n";
    else cout << "You win.\n"<< work << " exp eared.\n";
    return work;
}

int main()
{
    Baker Saveliy;
    Baker Username;
    for (int i = 0; i < 10; i++)
    {
        Saveliy.Create_roll();
        Username.Create_roll();
    }

    int f = 0;
    int total_exp;
    Bake(Saveliy, Username);
    while (f < 15)
    {
        int work = Bake(Saveliy, Username);
        if (work > 50) f++;
        total_exp += work;
    }

    Level_up(Saveliy, total_exp);
    Level_up(Username, total_exp);
    for (int i = 0; i < 10; i++)
    {
        Saveliy.Eat_roll();
        Username.Eat_roll();
    }
    return 0;
}
