#ifndef BAKER_H
#define BAKER_H

class Roll;
class Baker
{
    public:
        Baker();
        Baker(const Baker& other);
        void Create_roll();
        void Eat_roll();
        static void Give_roll(Baker& owner, Baker& target);
        int Get_current_roll();
        int Get_exp();
        int Get_level();
        void Set_exp(int value);
        void Set_level(int value);
        Roll Get_roll(int index);
    protected:
    private:
        Roll* _rolls;
        int _current_rolls;
        int _exp;
        int _fullness;
        int _level;
};

#endif // BAKER_H
