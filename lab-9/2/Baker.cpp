#include "Baker.h"
#include "Roll.cpp"
#include <iostream>
Baker::Baker()
{
    _rolls = new Roll [100];
    _exp = 0;
    _fullness = 100;
    _level = 1;
    _current_rolls = 0;
}

Baker::Baker(const Baker& other)
{
    _rolls = new Roll [100];
    for (int i = 0; i < other._current_rolls; i++)
    {
        this->_rolls[i] = other._rolls[i];
    }
    this->_current_rolls = other._current_rolls;
    this->_exp = other._exp;
    this->_level = other._level;
    this->_fullness = other._fullness;
}

void Baker::Create_roll()
{
    if (_fullness <= 2) std::cout << "Cannot create roll, too hungry!\n";
    else
    {
        Roll NewRoll(_level+(_fullness/10));
        _fullness -= 2;
        _rolls[_current_rolls] = NewRoll;
        std::cout << (_level+_fullness)<< "\n";
        _current_rolls++;
    }
}

void Baker::Eat_roll()
{
    _current_rolls--;
    Roll current = _rolls[_current_rolls];
    _fullness += current.Get_taste();
}

void Baker::Give_roll(Baker& owner, Baker& target)
{
    owner._current_rolls--;
    target._rolls[target._current_rolls] = owner._rolls[owner._current_rolls];
    target._current_rolls++;
}

int Baker::Get_current_roll()
{
    return _current_rolls;
}

Roll Baker::Get_roll(int index)
{
    return _rolls[index];
}

int Baker::Get_exp()
{
    return _exp;
}
int Baker::Get_level()
{
    return _level;
}
void Baker::Set_exp(int value)
{
    _exp = value;
}

void Baker::Set_level(int value)
{
    _level = value;
}
