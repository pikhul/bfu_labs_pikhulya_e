#include "Roll.h"
#include <ctime>
#include <cstdlib>
#define N 20

int Roll::counter;

Roll::Roll()
{
    counter++;
    _taste = 50;
    srand(time(NULL));
    int f = rand()%4;
    switch (f)
    {
    case 0:
        _name = "1";
        break;
    case 1:
        _name = "2";
        break;
    case 2:
        _name = "3";
        break;
    case 3:
        _name = "4";
        break;
    }
}

Roll::Roll(int taste, string name)
{
    _taste = taste;
    _name = name;
    counter ++;
}

Roll::Roll(int taste)
{
    _taste = taste;
    int f = rand()%4;
    switch (f)
    {
    case 0:
        _name = "1";
        break;
    case 1:
        _name = "2";
        break;
    case 2:
        _name = "3";
        break;
    case 3:
        _name = "4";
        break;
    }
    counter ++;
}

Roll::Roll(const Roll& other)
{
    _name = other._name;
    _taste = other._taste;
}



Roll Roll::operator = (const Roll &other)
{
	return Roll(this->_taste = other._taste, this->_name = other._name);
}


Roll Roll::Compare (Roll a, Roll b)
{
    if (a._taste > b._taste) return a;
    else return b;
}


int Roll::Get_taste()
{
    return _taste;
}
void Roll::Set_taste(int taste)
{
    _taste = taste;
}
string Roll::Get_name()
{
    return _name;
}
void Roll::Set_name(string name)
{
    _name = name;
}
