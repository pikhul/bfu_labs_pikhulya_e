#ifndef ROLL_H
#define ROLL_H
#include <string>

using namespace std;

class Roll
{
    public:
    static int counter;
    static Roll Compare(Roll a, Roll b);
        Roll();
        Roll (int taste, string name);
        Roll (int taste);
        //~Roll();
        Roll(const Roll& other);
        int Get_taste();
        void Set_taste(int taste);
        string Get_name();
        void Set_name(string name);
        Roll operator = (const Roll &other);
    protected:
    private:
    string _name;
    int _taste;
};

#endif // ROLL_H
