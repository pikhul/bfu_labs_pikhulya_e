#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define FILE_NAME_SIZE 25
#define PLANET_SIZE 10

int Quantity ()
{
    int i;
    for (i = FILE_NAME_SIZE; i> 0; i--)
    {
        char * fileName[FILE_NAME_SIZE];
        sprintf(fileName,"%0*d.txt",i,0);
        FILE * input = fopen(fileName,"r");
        if (input != NULL)
        {
            fclose (input);
            break;
        }
    }
    return i;
}

typedef unsigned long long int ULLI;


ULLI power (int a, int b)
{
    int i;
    int A = a;
    for (i = 1; i <b; i++) a = A*a;
    return a;
}


void FindPlanet (char * Planet)
{

    int Q = Quantity();
    ULLI N = power(10, Q);
    ULLI i;
    for (i = 0; i < N; i++)
    {
        char fileName[FILE_NAME_SIZE];
        sprintf(fileName,"%0*d.txt",Q,i);
        FILE * input = fopen(fileName,"r");
        if (input!=NULL)
        {
            char Data [PLANET_SIZE] = "0";
            fscanf(input,"%s",&Data);
            if (strcmp(Data,Planet) == 0)
            {
                printf("%s\n",fileName);
            }
            fclose(input);
        }
    }

}

int main(void){
	char Planet[PLANET_SIZE];
    scanf("%s", &Planet);
	FindPlanet(Planet);

	return 0;
}
