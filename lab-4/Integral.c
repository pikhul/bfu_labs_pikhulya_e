#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <math.h>

#define EPSILON 1000

typedef double (*fType) (double);

double f1 ( double x) {return sin(x);}

double f2 ( double x) {return (x*cos(x));}

double f3 ( double x) {return (x*x+x-tan(x));}

double f4 ( double x) {return (exp(x)+3);}



double Integral(double a, double b, double c, int n, fType f)
{
  int i;
  double result, h;

  result = 0;

  h = (b - a) / n; //��� �����

  for(i = 0; i < n; i++)
  {
    result += (*f)(a + h * (i + 0.5)); //��������� � ������� ����� � ��������� � �����
  }

  result *= h;

  result = result - c*(b-a);


  return result;
}




double Count (double S, double a, double b, double c)
{
    fType Function[]={f1,f2,f3,f4};
    int i;
    double maximum = 0;
    for (i = 0; i<4; i++)
    {
        double result = Integral(a, b, c, EPSILON, (fType)Function[i]);
        if ((result > maximum) && (result <= S)) maximum = result;
    }
    //printf(" no glavnoe chislo %lf \n", maximum);
    return maximum;

}


int main ()
{
    double a,b ,c ;
    double S ;
    scanf("%lf", &S);
    scanf("%lf %lf", &a,&b);
    scanf("%lf", &c);
    printf("%lf", Count(S,a,b,c));
    return 0;
}
