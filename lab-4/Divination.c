#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#define DAIRY_SIZE 10
#define STR_MAX_LEN 100

void delstr (char *** D, int n, int * Num)
{
    char ** Dairy = *D;
    int N = *Num;
    int i;
    N--;
    free(Dairy[n]);
    for (i = n; i < N; i++)
    {
        Dairy[i]=Dairy[i+1];
    }
    *Num = N;
    *D = Dairy;
}

int GetDairy (char *** D)
{
    char ** Dairy = malloc(sizeof(*Dairy)*DAIRY_SIZE);
    char* Buffer = malloc(sizeof(char)*STR_MAX_LEN);
    int len;
    int i = 0;
    do
    {
        gets(Buffer);
        Dairy[i] = malloc(sizeof(char)*strlen(Buffer));
        strcpy(Dairy[i++], Buffer);
    } while (strcmp(Buffer,"") !=0);
    *D = Dairy;
    free(Buffer);
    return i;
}

void PutDairy (char ** Dairy, int n)
{
    int i;
    for (i = 0; i<n; i++)
    printf("%s\n",(Dairy[i]));

}

void CompareDairies (char *** D1, char *** D2, int * N_1, int *N_2)
{
    int i, j;
    char ** Dairy1 = *D1;
    char ** Dairy2 = *D2;
    int N1 = *N_1;
    int N2 = *N_2;
    int f = 1;
    if (N1 > N2) f = 0;
    for (i = 0; i < N1; i++)
        for (j = 0; j < N2; j++)
        {
            if (strcmp(Dairy1[i], Dairy2[j]) == 0)
                if (f==0){ delstr(&Dairy1, i, &N1); f = 1;}
                else { delstr(&Dairy2, j, &N2); f = 0;}
        };
    *D1 = Dairy1;
    *D2 = Dairy2;
    *N_1 = N1;
    *N_2 = N2;
}



int main ()
{
    char ** RonsDairy;
    char ** HarrysDairy;
    int N = GetDairy(&RonsDairy);
    int M = GetDairy(&HarrysDairy);
    CompareDairies(&RonsDairy, &HarrysDairy, &N, &M);
    PutDairy(RonsDairy, N);
    printf("\n");
    PutDairy(HarrysDairy, M);
    return 0;
}
