// Long arithmetics calculator.
//Input:
//    Two digits and sign of operation
//Output:
//    Result of operation
//Sample:
//in
//    124658
//    +
//    325694
//out
//    450352

#include <stdio.h>
#include <stdlib.h>


#include <string.h>
#include <math.h>

#define max(a, b) (((a) > (b)) ? (a) : (b))

#define MX_VALUE 10000

typedef int* LongNum;

LongNum GetEmptyNum(const int size)
{
    LongNum num = (LongNum)malloc(sizeof(int)*MX_VALUE);
    memset(num, 0, size * sizeof(int));
    return num;
}

void ReadNum(LongNum num)
{
    char* buffer = (char*)malloc(MX_VALUE*sizeof(char));
    gets(buffer);
    num[0] = strlen(buffer);
    int i;
    int j;
    for (i=num[0]-1, j=1; i>=0; i--, j++)
    {
        num[j] = buffer[i] - '0';
    }
}

int CompareNum(LongNum num1, LongNum num2)
{
    if(num1[0] > num2[0])
    {
        return 1;
    }
    if(num1[0] < num2[0])
    {
        return -1;
    }
    int i;
    for(i = num1[0]; i > 0; i--)
    {
        if(num1[i] > num2[i])
            return 1;
        if(num1[i] < num2[i])
            return -1;
    }
    return 0;
}

void PrintNum(LongNum num)
{
    while (num[num[0]]==0 && num[0]!=1)
    {
        num[0]--;
    }
    int i;
    for (i = num[0]; i; i--)
    {
        printf("%c", num[i]+'0');
    }
    printf("\n");
}

LongNum CopyNum(LongNum num)
{
    LongNum buffer = GetEmptyNum(MX_VALUE);
    int i;
    for (i=0;i<=num[0];i++)
    {
        buffer[i]=num[i];
    }
    return buffer;
}

void ReverseNum(LongNum num)
{
    LongNum buffer = CopyNum(num);
    int i;
    for (i=buffer[0];i>0;i--)
    {
        num[i] = buffer[i];
    }
    free(buffer);
}

//result=num1+num2
LongNum AddNum(LongNum num1, LongNum num2)
{
    LongNum result = GetEmptyNum(MX_VALUE);
    int tmp;
    result[0] = max(num1[0],num2[0]);
    tmp = 0;
    int i;
    for (i=1; i<=result[0]; i++)
    {
        tmp+=(num1[i]+num2[i]);
        result[i] = tmp%10;
        tmp/=10;
    }
    if (tmp) result[++result[0]] = tmp;
    return result;
}

LongNum SubNum (LongNum num1, LongNum num2)
{
    LongNum result = GetEmptyNum(MX_VALUE);
    if (CompareNum(num1, num2) == -1)
    {
        LongNum tmp = num1;
        num1 = num2;
        num2 = tmp;
        printf("-");
    }
    result[0] = num1[0];
    int i;
    for (i=1;i<result[0]+1;i++)
    {
        if (i<result[0])
        {
            num1[i+1]--;
            result[i] += 10 + num1[i];
        }
        else
        {
            result[i] += num1[i];
        }
        result[i] -= num2[i];
        if (result[i] / 10 > 0)
        {
            result[i + 1]++;
            result[i] %= 10;
        }
    }
    return result;
}



//result=num1*num2
LongNum MultiplyNum(LongNum num1, LongNum num2)
{
    LongNum result = GetEmptyNum(MX_VALUE);
    int tmp;
    int i;
    for (i=1; i<=num1[0]; i++)
    {
        int j;
        for (j=1; j<=num2[0]; j++)
        {
            result[i+j-1]+=num1[i]*num2[j];
        }
    }
    result[0] = num1[0] + num2[0] - 1;
    for (i=1; i<=result[0]; i++)
    {
        result[i+1]+=result[i]/10;
        result[i]%=10;
    }
    tmp=result[result[0]+1];
    while (tmp)
    {
        result[++result[0]]=tmp%10;
        tmp/=10;
    }
    return result;
}


int main() {

    LongNum num1 = GetEmptyNum(MX_VALUE);
    LongNum num2 = GetEmptyNum(MX_VALUE);

    char c;
    int shortnum;

    ReadNum(num1);
    scanf("%c\n",&c);
    ReadNum(num2);

    switch ( c )
    {
        case '+':
            PrintNum(AddNum(num1,num2));
            break;
        case '*':
            PrintNum(MultiplyNum(num1, num2));
            break;
        case '-':
            PrintNum(SubNum(num1, num2));
            break;
        default:
            printf("Unknown operation");
            break;
    }

    free (num1);
    free (num2);

    return 0;
}

