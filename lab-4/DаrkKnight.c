#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#define N 10
#define RANDOM 11

void CreateArr ( int** const A)
{
    *A = malloc(sizeof(int)*N);
    srand (time(NULL));
    int i;
    for (i = 0; i < N; i++)
        (*A)[i] =  rand() % RANDOM;
}

void Life ( int* const H, int* Food, int* Sport)
{
    int Human = *H;
    int i = 0, j = 0;
    srand (time(NULL));
    int FrekenBock;
    while ((Human < 100 ) && (Human > 40))
    {
        FrekenBock = rand() % 2;
        if (FrekenBock == 1)
        {
            Human -= Sport[j];
            if (Human <= 40) {printf("Carlson died of hunger. Thieves got all the sheets."); break;}
            j++;
            if (j == N){printf("All thieves are defeated!"); break;}
        }
        else
        {
            Human += Food[i];
            if (Human >= 100){printf("Carlson could not reach the roof. Thieves got all the sheets."); break;}
            i++;
            if (i == N) {printf("Run out of jam."); break;}
        }
    }

    *H = Human;
}

int main ()
{
    int Carlson = 60;
    int * Jam;
    int * Thieves;
    CreateArr(&Jam);
    CreateArr(&Thieves);
    Life(&Carlson, Jam, Thieves);
    free(Jam);
    free(Thieves);
    return 0;
}
