#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <stdio.h>
#include <signal.h>



int f = 0;

void Joke(){
	int a = rand()%3;
	switch (a)
	{
		case 0 :
			printf("Joke 1\n");
		case 1:
			printf("Joke 2\n");
		case 2:
			printf("Joke 3\n");
	}
}

void signalfn(int sig) {
	pid_t p;
	int status;
	while ((p=wait(&status)) != -1) {
		printf("Completed!\n");
		f = 1;
	}
}

int main(int argc, char* argv[]){
	srand(time(NULL));
	pid_t pid = fork();
	if (pid) {
		printf("Start!\n");
		signal(SIGCHLD,signalfn);
		while (!f){
			Joke();
			usleep(5000);
		}
	}else{
		execl("/gpfs/home/epikhulya/lab-6/2/a.out", "", (char*)NULL);
	}
	return 0;
}
