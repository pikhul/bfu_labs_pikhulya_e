#include <unistd.h>
#include <stdio.h>
#include <fcntl.h>
#include <string.h>
#include <stdlib.h>

int cmp(const void* buf1,const void* buf2){
	return strcmp(*(char**)(buf1), *(char**)(buf2));
}

int main(void){
	FILE* in = fopen("input.txt", "r");
	
	char** text = (char**)malloc(sizeof(char*)*100000);
	int size = 0;

	char* string = (char*)malloc(sizeof(char)*100);

	while ((string=fgets(string, 100*sizeof(char), in))!=NULL)
        {
		text[size] = (char*)malloc(sizeof(char)*100);
		strncpy(text[size], string, sizeof(char)*100);
                size++;	
	}
	qsort(text, size, sizeof(char*), cmp);
	FILE* out = fopen("output.txt", "w");
	int i;
	for (i=0;i<size;i++)
        {
		fputs(text[i], out);
	}
	return 0;
}
