#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>

int main(){
	int fd;
	char* fifo = "/gpfs/home/epikhulya/lab-6/3/my_named_pipe";
	char buf[1024];
	fd = open(fifo, O_RDONLY, 0666);
	printf("fd=%d\n",fd);
	read(fd, buf, 1024);
	int i;
	for (i=0;i<10;i++)
		printf("%s\n", buf);
	close(fd);
	return 0;
}
