#include <fcntl.h>
#include <sys/stat.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>

int main(){
	int fd;
	char* fifo = "/gpfs/home/epikhulya/lab-6/3/my_named_pipe";
	unlink(fifo);
	mkfifo(fifo, 0666);
        fd = open(fifo, O_WRONLY);
	char msg[100];
	gets(msg);
        write(fd, msg, strlen(msg));
	close(fd);
	unlink(fifo);
	return 0;
}

