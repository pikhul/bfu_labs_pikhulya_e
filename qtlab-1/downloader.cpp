#include "downloader.h"

downloader::downloader(QObject *parent) : QObject(parent)
{
}

void downloader::do_donload(QString city)
{
    manager = new QNetworkAccessManager(this);
    connect(manager, SIGNAL(finished(QNetworkReply*)), this,SLOT(replyFinished(QNetworkReply*)));
    QString sample="http://samples.openweathermap.org/data/2.5/weather?q=London,uk&appid=b6907d289e10d714a6e88b30761fae22";
    QString url = "http://openweathermap.org/data/2.5/weather?q="+city+"&appid=b6907d289e10d714a6e88b30761fae22";
    manager->get(QNetworkRequest(QUrl(url)));
}

void downloader::replyFinished(QNetworkReply * reply)
{
    QTextStream cout (stdout);
    QByteArray data= reply->readAll();
    if (data.at(0) == '{')
    {
        QJsonDocument document = QJsonDocument::fromJson(data);
        QJsonObject root = document.object();
        QJsonValue code = root.value("cod");
        if (code.toInt() != 401)
        {
            cout << "Weather in " << root.value("name").toString()<< "\n";
            QJsonObject weather = root.value("main").toObject();
            for (int i = 0; i < weather.count(); i++)
            {
                if ( weather.value(weather.keys().at(i)).isDouble())
                    cout << weather.keys().at(i) << " :" << weather.value(weather.keys().at(i)).toDouble()<< "\n";
                else
                    cout << weather.keys().at(i) << " :" << weather.value(weather.keys().at(i)).toInt()<< "\n";
            }
        }
        cout << "\n";
    }
    else
        cout << "This city does not exist. \n";
}


