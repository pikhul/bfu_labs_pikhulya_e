#include <iostream>
#include <thread>
#include <vector>
#include <ctime>

using namespace std;

class thread_guard
{
private:
    vector <thread> t;
    int n;
public:
    thread_guard (){n = 0;};
    void push(thread in)
    {
        t.push_back(move(in));
        n++;
    }
    void join()
    {
        for (int i = 0; i < n ; i++)
        {
            if (t[i].joinable()) t[i].join();
        }
    }
    ~thread_guard()
    {
        join();
    }
    thread_guard (thread_guard& in) = delete;
    thread_guard& operator = (thread_guard& in) = delete;
};

void mult ( vector <int> & x, vector <int> & y, int & res, int i1, int i2)
{
    res = 0;
    for (int i = i1; i < i2; i++)
    {
        res += x[i]*y[i];
    }
}



int main() {
    srand(time(NULL));
    int const n = 100000000;
    int NT = thread::hardware_concurrency();
    if(NT<2)
        NT=2;
    NT = 1;
    auto start = chrono::steady_clock::now();
    vector<int> x;
    vector<int> y;
    vector<int> res;
    x.assign(n,0);
    y.assign(n, 0);
    res.assign(NT, 0);
    for (int i = 0; i < n; i++) {
        x[i] = rand()%10;
    }
    for (int i = 0; i < n; i++) {
        y[i] = rand()%10;
    }
    int N_MAX = (n < NT ? n : NT);

    thread_guard worker;
    int index = 0;
    for (int i = 0; i < N_MAX; i++)
    {
        worker.push(thread(mult, ref(x), ref(y), ref(res[i]), index, index + n / NT + 1));
        index += n / NT + 1;
    }
    worker.join();
    long long int sum = 0;
    for (int i = 0; i < NT; i++)
    {
        sum += res[i];
    }
    auto end = chrono::steady_clock::now();
    auto diff = end - start;
    cout << sum << endl;
    cout << chrono::duration <double, milli> (diff).count() << " ms" << endl;

    return 0;
}